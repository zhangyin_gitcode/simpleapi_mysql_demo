create database if not exists todo;
use todo;

create table if not exists todos (
    id int primary key auto_increment,
    content varchar(2000) not null
);