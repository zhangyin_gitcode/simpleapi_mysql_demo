FROM ubuntu:22.04
RUN apt update && apt install -y git build-essential libssl-dev
WORKDIR /app
COPY . .
RUN chmod +x entrypoint.sh
ENTRYPOINT ["/app/entrypoint.sh"]